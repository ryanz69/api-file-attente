<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;

/**
* @Route("/api/user", name="api_user")
*/
class UserController extends AbstractController
{
    private $serializer;
    public function __construct()
    {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(1);
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });
        $this->serializer = new Serializer([$normalizer], [$encoder]);
    }

    /**
    * @Route("/", methods="GET")
    */
    public function getAll(UserRepository $repo)
    {
        $data = $repo->findAll();
        
        $jsonContent = $this->serializer->serialize($data, "json");
        return JsonResponse::fromJsonString($jsonContent);
    }

    /**
    * @Route("/add", methods="POST")
    */
    public function add(Request $request, ObjectManager $manager, UserRepository $repo)
    {
        $content = json_decode($request->getContent(), true);
        $entityAdd = new User();
        $entityAdd->setName($content["name"]);
        $manager->persist($entityAdd);
        $manager->flush();
        $jsonContent = $this->serializer->serialize($entityAdd->getId(), "json");

        $client = new Client(new Version1X('http://192.168.1.61:8000'));
        $client->initialize();
        $client->close();

        return JsonResponse::fromJsonString($jsonContent);
    }

    /**
    * @Route("/admin/remove/{id}", methods="DELETE")
    */
    public function remove(User $entity, ObjectManager $manager, UserRepository $repo) {
        $manager->remove($entity);
        $manager->flush();
        $jsonContent = $this->serializer->serialize($entity->getId(), "json");
        $client = new Client(new Version1X('http://192.168.1.61:8000'));
        $client->initialize();
        $client->close();

        return JsonResponse::fromJsonString($jsonContent);
    }
}
